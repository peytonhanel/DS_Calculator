public class Test
{

   public static void main(String[] args)
      throws Exception
   {
      String[] arguments;
      for (int i = 0; i < 12; i++) {
         switch (i) {
            case 0:
               arguments = new String[] {"10 + -5"};
               execute(arguments);
               break;

            case 1:
               arguments = new String[] {"10 + 5 * 3"};
               execute(arguments);
               break;

            case 2:
               arguments = new String[] {"10 * 5 + 3"};
               execute(arguments);
               break;

            case 3:
               arguments = new String[] {"10 * ( 5 + 3 )"};
               execute(arguments);
               break;

            case 4:
               arguments = new String[] {"10 / 3 / 3"};
               execute(arguments);
               break;

            case 5:
               arguments = new String[] {"10 - 3 * 2"};
               execute(arguments);
               break;

            case 6:
               arguments = new String[] {"10 * ( 5 + 3 ) ^ 2 / 4"};
               execute(arguments);
               break;

            case 7:
               arguments = new String[] {"10 * 5 + 3 )"};
               execute(arguments);
               break;

            case 8:
               arguments = new String[] {"10 * ^ 5 + 3"};
               execute(arguments);
               break;

            case 9:
               arguments = new String[] {"10 * 5 +"};
               execute(arguments);
               break;

            case 10:
               arguments = new String[] {"10 * 5 6"};
               execute(arguments);
               break;

            case 11:
               arguments = new String[] {"10 * 5 + foobar"};
               execute(arguments);
               break;
         }
      }
   }

   static void execute(String[] arguments)
      throws Exception
   {
      for (int j = 0; j < 3; j++) {
         if (j == 0) {
            System.out.println("\n\nCompute_TK8cFCdA " + "\"" + arguments[0] + "\"");
            Compute_TK8cFCdA.main(arguments);
         }
         else if (j == 1) {
            System.out.println("\nCompute_UFA9iw8Q " + "\"" + arguments[0] + "\"");
            Compute_UFA9iw8Q.main(arguments);
         }
         else if (j == 2) {
            System.out.println("Compute_YbKLXpzJ " + "\"" + arguments[0] + "\"");
            Compute_YbKLXpzJ.main(arguments);
         }
      }
   }
}
