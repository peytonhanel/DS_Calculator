import java.util.*;

/**
 * A program that takes in a string from the command line of operands and
 * operators and calculates an answer.
 *
 * @Author Peyton Hanel
 */
public class Compute
{

   private static final int ADDITION_PRIORITY = 1;
   private static final int MULTIPLICATION_PRIORITY = 2;
   private static final int EXPONENTIATION_PRIORITY = 3;

   private static final int ADDITION_INDEX = 0;
   private static final int SUBTRACTION_INDEX = 1;
   private static final int MULTIPLICATION_INDEX = 2;
   private static final int DIVISION_INDEX = 3;
   private static final int EXPONENTIATION_INDEX = 4;
   private static final int OPEN_PARENTHESIS_INDEX = 5;
   private static final int CLOSED_PARENTHESIS_INDEX = 6;

   private static final String[] OPERATORS =
      { "+", "-", "*", "/", "^", "(", ")" };


   /**
    * The main method of this program.
    *
    * @param args Command line arguments.
    */
   public static void main (String[] args)
   {
      System.out.println(calculateAnswer(CreatePostfix(args[0])));
   }


   /**
    * Takes in an infix list and turns it into an RPN postfix list.
    *
    * @param infixList The infix list.
    * @return A String that is the postfix list.
    */
   private static LinkedList<String> CreatePostfix(String infixList)
   {
      StringTokenizer infixTokenList = new StringTokenizer(infixList);
      Deque<String> operatorStack = new LinkedList<>();
      LinkedList<String> postfixList = new LinkedList<>();

      // turns the infix list into an RPN postfix list
      while (infixTokenList.hasMoreTokens())
      {
         String currentToken = infixTokenList.nextToken();
         // adds operators to the postfix list
         if (Arrays.asList(OPERATORS).contains(currentToken))
         {
            // always pushes opening parenthesis to operator stack
            if (currentToken.equals(OPERATORS[OPEN_PARENTHESIS_INDEX])) {
               operatorStack.push(currentToken);
            }
            // if the currentToken is a closing parenthesis, pop every operator from the operator
            // stack and add it to the postfix until an opening parenthesis is found.
            else if (currentToken.equals(OPERATORS[CLOSED_PARENTHESIS_INDEX])) {
               while (!operatorStack.isEmpty() && !operatorStack.peek().equals(OPERATORS[OPEN_PARENTHESIS_INDEX])) {
                  postfixList.add(operatorStack.pop());
               }
               // when the opening parenthesis is found, get rid of it. We do not
               // add parentheses to the postfixList.
               if (!operatorStack.isEmpty() && operatorStack.peek().equals(OPERATORS[OPEN_PARENTHESIS_INDEX])) {
                  operatorStack.pop();
               }
               // else there is no opening parenthesis and we need to quit.
               else {
                  failAndExit("No opening parenthesis found.");
               }
            }
            // for normal operators, add them to the stack or the postfix based on priority.
            else {
               // while the operator at the top of the operatorStack has a higher priority, pop it and add it to the postfix list.
               while (!operatorStack.isEmpty() && getOperatorPriority(operatorStack.peek()) > getOperatorPriority(currentToken)) {
                  postfixList.add(operatorStack.pop());
               }
               operatorStack.push(currentToken);
            }
         }
         // if the current token is an operand, add it to the postfixList
         else {
            postfixList.add(currentToken);
         }
      }
      // adds any operators left in the operatorStack to the postfix list.
      while (!operatorStack.isEmpty()) {
         postfixList.add(operatorStack.pop());
      }

      return postfixList;
   }


   /**
    * Takes in an RPN postfix list and calculates it.
    *
    * @param postfix The postfix list.
    * @return The result of the list.
    */
   private static int calculateAnswer(LinkedList<String> postfix)
   {
      Deque<Integer> operandStack = new LinkedList<>();

      // for each item in the postfix list
      for (String currentToken : postfix) {
         // if currentToken is an operator, pop two operands and compute them with operator
         if (Arrays.asList(OPERATORS).contains(currentToken)) {
            // catches incorrect syntax with operators (i.e. there are two + + in a row)
            try {
               operandStack.push(calculateSingleStatement(operandStack.pop(), operandStack.pop(), currentToken));
            }
            catch (NoSuchElementException e) {
               failAndExit("Incorrect syntax.");
            }
         }
         // if the current token is an operand, add it to the operand stack.
         else {
            try {
               operandStack.push(Integer.parseInt(currentToken));
            }
            catch (NumberFormatException e) {
               failAndExit(currentToken + " is not a recognized operator");
            }
         }
      }
      // catches when there is no operator between an operand and a parenthesis (i.e. "5 (3 + 4)")
      if (operandStack.size() > 1) {
         failAndExit("Incorrect syntax.");
      }
      // the answer is on the top
      return operandStack.pop();
   }


   /**
    * Calculates a statement.
    *
    * @param operand2 The second operand.
    * @param operand1 The first operand.
    * @param operatorToken The arithmetic operation that will be applied to these operands.
    * @return The result of the calculation.
    */
   private static int calculateSingleStatement(int operand2, int operand1, String operatorToken)
   {
      int result = 0;

      // addition
      if (operatorToken.equals(OPERATORS[ADDITION_INDEX])) {
         result = operand1 + operand2;
      }
      // subtraction
      else if (operatorToken.equals(OPERATORS[SUBTRACTION_INDEX])) {
         result = operand1 - operand2;
      }
      // multiplication
      else if (operatorToken.equals(OPERATORS[MULTIPLICATION_INDEX])) {
         result = operand1 * operand2;
      }
      // division
      else if (operatorToken.equals(OPERATORS[DIVISION_INDEX])) {
         result = operand1 / operand2;
      }
      // exponentiation
      else if (operatorToken.equals(OPERATORS[EXPONENTIATION_INDEX])) {
         result = (int)Math.pow(operand1, operand2);
      }
      // this means that there was no closing parenthesis found
      else if (operatorToken.equals(OPERATORS[OPEN_PARENTHESIS_INDEX])) {
         failAndExit("No closing parenthesis found.");
      }
      // operatorToken is not a recognized operator.
      else {
         failAndExit(operatorToken + " is not a recognized operator");
      }
      return result;
   }


   /**
    * Takes an operator as a parameter and returns its priority value.
    *
    * @param operator A String that contains an operator symbol, as defined by OPERATORS
    * @return An int representing the priority. Higher values have higher priority.
    */
   private static int getOperatorPriority(String operator)
   {
      int priority = 0; // open parentheses are assigned 0;

      // + or - operators
      if (operator.equals(OPERATORS[ADDITION_INDEX]) || operator.equals(OPERATORS[SUBTRACTION_INDEX])) {
         priority = ADDITION_PRIORITY;
      }
      // * or / operators
      else if (operator.equals(OPERATORS[MULTIPLICATION_INDEX]) || operator.equals(OPERATORS[DIVISION_INDEX])) {
         priority = MULTIPLICATION_PRIORITY;
      }
      // ^ operator
      else if (operator.equals(OPERATORS[EXPONENTIATION_INDEX])) {
         priority = EXPONENTIATION_PRIORITY;
      }
      // if the operator is incorrect
      else if (!operator.equals(OPERATORS[OPEN_PARENTHESIS_INDEX]) && !operator.equals(OPERATORS[CLOSED_PARENTHESIS_INDEX])) {
         failAndExit(operator + " is not a recognized operator");
      }
      return priority;
   }


   /**
    * Prints an error message to System.err and exits the program.
    *
    * @param errorMessage The message to print.
    */
   private static void failAndExit(String errorMessage)
   {
      System.err.println(errorMessage);
      System.exit(1);
   }
}
